import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:wishlisttracker/animation/fadeIn.dart';
import 'package:wishlisttracker/models/notifications.dart';
import 'package:wishlisttracker/widgets/profile.dart';

class UserDetails extends StatefulWidget {
  final String userInfoId;
  UserDetails(this.userInfoId, {Key key}) : super(key: key);
  @override
  _UserDetailsState createState() => _UserDetailsState();
}

class _UserDetailsState extends State<UserDetails> {
  Notifications objNotification = new Notifications();

  @override
  void initState() {
    super.initState();
    Future.microtask(() {
      print("Inside UserDetails");
      Provider.of<Notifications>(context, listen: false)
          .getUserNotifications(widget.userInfoId);
    });
  }

  List<dynamic> supportedVendor = [
    {"1": "1"},
    {"2": "2"},
    {"1": "1"},
    {"2": "2"},
    {"1": "1"},
    {"2": "2"},
  ];

  Widget _backNavigation() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.arrow_back),
            iconSize: 30.0,
            color: Colors.white,
            onPressed: () => Navigator.pop(context),
          ),
        ],
      ),
    );
  }

  Widget getVendorChip(UserWishList wishType) {
    return FadeIn(
      0.8,
      Container(
        width: 100.0,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
          color: Color(wishType.masterWebsiteInfo.brandColor), // border color
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              wishType.masterWebsiteInfo.title,
              style: TextStyle(
                fontSize: 19.0,
                fontWeight: FontWeight.w500,
                color: Theme.of(context).accentColor,
              ),
            ),
            Text(
              wishType.wishCount.toString(),
              style: TextStyle(
                  fontSize: 21,
                  color: Theme.of(context).accentColor,
                  fontWeight: FontWeight.w600),
            )
          ],
        ),
      ),
    );
  }

  Widget _profilePic() {
    return Center(
      child: Profile(true),
    );
  }

  Widget _profileName() {
    return Center(
      child: Text(
        "Anuj Gupta",
        style: TextStyle(
            color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.w500),
      ),
    );
  }

  Widget _productWithCount() {
    return Column(
      children: <Widget>[
        Container(
            height: 80.0,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(left: 5.0),
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: objNotification.userWishList != null
                    ? objNotification.userWishList.length
                    : 0,
                itemBuilder: (context, index) {
                  var wishType = objNotification.userWishList[index];
                  // var vendorType = supportedVendor[index];
                  return Padding(
                    padding: EdgeInsets.only(left: 7.0),
                    child: getVendorChip(wishType),
                  );
                })),
      ],
    );
  }

  Widget _notificationPannel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 15.0, top: 5.0),
          child: Text(
            "Notification History",
            style: TextStyle(
                color: Colors.white,
                fontSize: 19.0,
                fontWeight: FontWeight.w500),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(right: 15.0, top: 5.0),
          child: Icon(
            Icons.notifications_active,
            color: Colors.white,
          ),
        )
      ],
    );
  }

  Widget _notification(UserNotification obj) {
    return FadeIn(
      0.8,
      Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
            height: 90.0,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.white, // border color
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 25.0, right: 2.0),
                  child: Text(
                    // "Congo Price drop Realme 6 Pro (Lightning Blue, 64 GB)  (6 GB RAM) 😎",
                    obj.title,
                    style: TextStyle(
                      fontSize: 17.0,
                      fontWeight: FontWeight.w500,
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 25.0, right: 2.0),
                  child: Text(
                    obj.message,
                    style:
                        TextStyle(color: Colors.grey.shade700, fontSize: 16.0),
                  ),
                )
              ],
            ),
            // Text("Congo Price drop Realme 6 Pro (Lightning Blue, 64 GB)  (6 GB RAM) 😎"),
          ),
          // Padding(
          //   padding: EdgeInsets.only(top: 20.0),
          //   child: MaterialButton(
          //     onPressed: () {},
          //     color: Colors.yellow,
          //     textColor: Colors.white,
          //     child: Text("A",
          //         style: TextStyle(
          //           fontSize: 22.0,
          //           color: Theme.of(context).accentColor,
          //         )),
          //     padding: EdgeInsets.all(15),
          //     shape: CircleBorder(),
          //   ),
          // )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    objNotification =
        Provider.of<Notifications>(context, listen: true).getUserObjection;
    print("Notification $objNotification");

    return Scaffold(
      backgroundColor: Theme.of(context).accentColor,
      body: Column(
        children: <Widget>[
          _backNavigation(),
          _profilePic(),
          _profileName(),
          SizedBox(
            height: 15.0,
          ),
          _productWithCount(),
          _notificationPannel(),
          Expanded(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: objNotification.userNotification != null
                    ? objNotification.userNotification.length
                    : 0,
                itemBuilder: (context, index) {
                  var obj = objNotification.userNotification[index];
                  return _notification(obj);
                }),
          ),
        ],
      ),
    );
  }
}
