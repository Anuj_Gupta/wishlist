import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  final bool isDetails;
  Profile(this.isDetails, {Key key}) : super(key: key);
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(
            top: widget.isDetails ? 0.0 : 5.0, left: 15, right: 10),
        child: Container(
            width: widget.isDetails ? 100 : 40,
            height: widget.isDetails ? 100 : 40,
            decoration: BoxDecoration(
              border: Border.all(
                color: Theme.of(context).scaffoldBackgroundColor,
                style: BorderStyle.solid,
                width: widget.isDetails ? 3.0 : 1.0,
              ),
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Container(
              padding: EdgeInsets.only(bottom: 2.0),
              width: 40,
              height: 40,
              child: Image.asset('assets/users.png'),
            )));
  }
}
