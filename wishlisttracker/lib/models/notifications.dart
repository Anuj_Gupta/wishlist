import 'package:flutter/widgets.dart';
import 'package:wishlisttracker/utility/apiCalling.dart';
import 'dart:convert';

class Notifications extends ChangeNotifier {
  Notifications objNotification;

  List<UserNotification> userNotification;
  List<UserWishList> userWishList;

  Notifications({this.userNotification, this.userWishList});

  Notifications get getUserObjection =>
      objNotification != null ? objNotification : new Notifications();

  Notifications.fromJson(Map<String, dynamic> jsonObj) {
    if (jsonObj['userNotification'] != null) {
      userNotification = new List<UserNotification>();
      dynamic temp = json.decode(jsonObj['userNotification']);
      temp.forEach((v) {
        userNotification.add(new UserNotification.fromJson(v));
      });
    }
    if (jsonObj['userWishList'] != null) {
      userWishList = new List<UserWishList>();
      dynamic temp = json.decode(jsonObj['userWishList']);
      temp.forEach((v) {
        userWishList.add(new UserWishList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userNotification != null) {
      data['userNotification'] =
          this.userNotification.map((v) => v.toJson()).toList();
    }
    if (this.userWishList != null) {
      data['userWishList'] = this.userWishList.map((v) => v.toJson()).toList();
    }
    return data;
  }

  void getUserNotifications(userInfoId) async {
    var reqBody = {"userInfoId": userInfoId};
    var dynamicBody = await ApiCalling.postReq('getNotification', reqBody);
    if (dynamicBody != null) {
      Notifications obj = Notifications.fromJson(dynamicBody);
      objNotification = new Notifications();
      objNotification.userNotification = obj.userNotification;
      objNotification.userWishList = obj.userWishList;
      notifyListeners();
    }

    print("Got history");
  }
}

class UserNotification {
  Id iId;
  Id userInfoId;
  String title;
  String action;
  String message;
  bool isSuccess;
  CreatedAt createdAt;
  Id userWishlistId;

  UserNotification(
      {this.iId,
      this.userInfoId,
      this.title,
      this.action,
      this.message,
      this.isSuccess,
      this.createdAt,
      this.userWishlistId});

  UserNotification.fromJson(Map<String, dynamic> json) {
    iId = json['_id'] != null ? new Id.fromJson(json['_id']) : null;
    userInfoId =
        json['userInfoId'] != null ? new Id.fromJson(json['userInfoId']) : null;
    title = json['title'];
    action = json['action'];
    message = json['message'];
    isSuccess = json['isSuccess'];
    createdAt = json['createdAt'] != null
        ? new CreatedAt.fromJson(json['createdAt'])
        : null;
    userWishlistId = json['userWishlistId'] != null
        ? new Id.fromJson(json['userWishlistId'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.iId != null) {
      data['_id'] = this.iId.toJson();
    }
    if (this.userInfoId != null) {
      data['userInfoId'] = this.userInfoId.toJson();
    }
    data['title'] = this.title;
    data['action'] = this.action;
    data['message'] = this.message;
    data['isSuccess'] = this.isSuccess;
    if (this.createdAt != null) {
      data['createdAt'] = this.createdAt.toJson();
    }
    if (this.userWishlistId != null) {
      data['userWishlistId'] = this.userWishlistId.toJson();
    }
    return data;
  }
}

class Id {
  String oid;

  Id({this.oid});

  Id.fromJson(Map<String, dynamic> json) {
    oid = json['$oid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['$oid'] = this.oid;
    return data;
  }
}

class CreatedAt {
  int date;

  CreatedAt({this.date});

  CreatedAt.fromJson(Map<String, dynamic> json) {
    date = json['$date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['$date'] = this.date;
    return data;
  }
}

class UserWishList {
  Id iId;
  int wishCount;
  String domainName;
  List<Wishes> wishes;
  MasterWebsiteInfo masterWebsiteInfo;

  UserWishList(
      {this.iId,
      this.wishCount,
      this.domainName,
      this.wishes,
      this.masterWebsiteInfo});

  UserWishList.fromJson(Map<String, dynamic> json) {
    iId = json['_id'] != null ? new Id.fromJson(json['_id']) : null;
    wishCount = json['wishCount'];
    domainName = json['domainName'];
    if (json['wishes'] != null) {
      wishes = new List<Wishes>();
      json['wishes'].forEach((v) {
        wishes.add(new Wishes.fromJson(v));
      });
    }
    masterWebsiteInfo = json['masterWebsiteInfo'] != null
        ? new MasterWebsiteInfo.fromJson(json['masterWebsiteInfo'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.iId != null) {
      data['_id'] = this.iId.toJson();
    }
    data['wishCount'] = this.wishCount;
    data['domainName'] = this.domainName;
    if (this.wishes != null) {
      data['wishes'] = this.wishes.map((v) => v.toJson()).toList();
    }
    if (this.masterWebsiteInfo != null) {
      data['masterWebsiteInfo'] = this.masterWebsiteInfo.toJson();
    }
    return data;
  }
}

class Wishes {
  Id iId;
  Id userInfoId;
  Id masterWebsiteId;
  String domainName;
  String websiteUrl;
  String name;
  String scrapePrice;
  String currentPrice;
  String currentRating;
  List<String> targetPrice;
  CreatedAt validTillDate;
  bool pushNotification;
  List<Null> wishImages;
  List<Null> negativeReview;
  List<Null> positiveReview;
  bool isActive;
  CreatedAt createdAt;

  Wishes(
      {this.iId,
      this.userInfoId,
      this.masterWebsiteId,
      this.domainName,
      this.websiteUrl,
      this.name,
      this.scrapePrice,
      this.currentPrice,
      this.currentRating,
      this.targetPrice,
      this.validTillDate,
      this.pushNotification,
      this.wishImages,
      this.negativeReview,
      this.positiveReview,
      this.isActive,
      this.createdAt});

  Wishes.fromJson(Map<String, dynamic> json) {
    iId = json['_id'] != null ? new Id.fromJson(json['_id']) : null;
    userInfoId =
        json['userInfoId'] != null ? new Id.fromJson(json['userInfoId']) : null;
    masterWebsiteId = json['masterWebsiteId'] != null
        ? new Id.fromJson(json['masterWebsiteId'])
        : null;
    domainName = json['domainName'];
    websiteUrl = json['websiteUrl'];
    name = json['name'];
    scrapePrice = json['scrapePrice'];
    currentPrice = json['currentPrice'];
    currentRating = json['currentRating'];
    targetPrice = json['targetPrice'].cast<String>();
    validTillDate = json['validTillDate'] != null
        ? new CreatedAt.fromJson(json['validTillDate'])
        : null;
    pushNotification = json['pushNotification'];
    isActive = json['isActive'];
    createdAt = json['createdAt'] != null
        ? new CreatedAt.fromJson(json['createdAt'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.iId != null) {
      data['_id'] = this.iId.toJson();
    }
    if (this.userInfoId != null) {
      data['userInfoId'] = this.userInfoId.toJson();
    }
    if (this.masterWebsiteId != null) {
      data['masterWebsiteId'] = this.masterWebsiteId.toJson();
    }
    data['domainName'] = this.domainName;
    data['websiteUrl'] = this.websiteUrl;
    data['name'] = this.name;
    data['scrapePrice'] = this.scrapePrice;
    data['currentPrice'] = this.currentPrice;
    data['currentRating'] = this.currentRating;
    data['targetPrice'] = this.targetPrice;
    if (this.validTillDate != null) {
      data['validTillDate'] = this.validTillDate.toJson();
    }
    data['pushNotification'] = this.pushNotification;
    data['isActive'] = this.isActive;
    if (this.createdAt != null) {
      data['createdAt'] = this.createdAt.toJson();
    }
    return data;
  }
}

class MasterWebsiteInfo {
  Id iId;
  String websiteName;
  String title;
  String domainName;
  String url;
  String priceClass;
  String priceId;
  String imagesClass;
  String reviewClass;
  String ratingClass;
  String nameId;
  int brandColor;
  String nameClass;
  bool isActive;

  MasterWebsiteInfo(
      {this.iId,
      this.websiteName,
      this.title,
      this.domainName,
      this.url,
      this.priceClass,
      this.priceId,
      this.imagesClass,
      this.reviewClass,
      this.ratingClass,
      this.nameId,
      this.brandColor,
      this.nameClass,
      this.isActive});

  MasterWebsiteInfo.fromJson(Map<String, dynamic> json) {
    iId = json['_id'] != null ? new Id.fromJson(json['_id']) : null;
    websiteName = json['websiteName'];
    title = json['title'];
    domainName = json['domainName'];
    url = json['url'];
    priceClass = json['priceClass'];
    priceId = json['priceId'];
    imagesClass = json['imagesClass'];
    reviewClass = json['reviewClass'];
    ratingClass = json['ratingClass'];
    nameId = json['nameId'];
    brandColor = int.tryParse(json['brandColor']);
    nameClass = json['nameClass'];
    isActive = json['isActive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.iId != null) {
      data['_id'] = this.iId.toJson();
    }
    data['websiteName'] = this.websiteName;
    data['title'] = this.title;
    data['domainName'] = this.domainName;
    data['url'] = this.url;
    data['priceClass'] = this.priceClass;
    data['priceId'] = this.priceId;
    data['imagesClass'] = this.imagesClass;
    data['reviewClass'] = this.reviewClass;
    data['ratingClass'] = this.ratingClass;
    data['nameId'] = this.nameId;
    data['brandColor'] = this.brandColor;
    data['nameClass'] = this.nameClass;
    data['isActive'] = this.isActive;
    return data;
  }
}
