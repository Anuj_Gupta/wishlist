import requests
import json
import os
registration_token = os.getenv("FIREBASEID")


def send_to_token(firebaseId, title, msg):
    data = {"notification": {"title": title, "body": msg,
                             "click_action": 'FLUTTER_NOTIFICATION_CLICK'}, "to": firebaseId}
    data_json = json.dumps(data)
    print(data_json)
    headers = {'Content-type': 'application/json',
               'Authorization': registration_token}

    url = 'https://fcm.googleapis.com/fcm/send'

    response = requests.post(url, data=data_json, headers=headers)

    jsonResponse = json.loads(response.content)
    print(jsonResponse)
